## Autor:Andrea Calleja
## ChangeLog
## v5.0 2020-12-29

### Added
* Conexión mediante Socket entre cliente y servidor
* Eliminadas las fifos de mundo cliete y mundo servidor
## v4.0 2020-12-16

### Added
* Hemos implementado los programas cliente y servidor, el cliente será el programa donde se controlen las teclas y el que estrá comunicado con el bot y el servidor enviará mediante una FIFO datos al cliente, estando este conectado a su vez con el logger.
* Incorporación de señales propuestas: SIGINT, SIGPIPE, SIGTERM y SIGUSR2.

### Modificated
* bot.cpp y logger.cpp para implementar los nuevos programas incorporados al juego.
* CMakeLists.txt para generar los ejecutables necesarios
* tenis.cpp, Mundo.cpp y Mundo.h han sido eliminados.
## v3.0 2020-12-01

### Added
* bot.cpp controlará la raqueta del jugador 1 y si el jugador 2 no ha pulsado ninguna tecla en 10 segundos, también podrá controlar la raqueta del jugador 2.
* logger.cpp un ejecutable que se comunicará con Mundo a través de FIFOtenis.
* DatosMemCompartida.h necesaria para la comunicación entre bot y Mundo.

### Modificated
* CMakeLists.txt para implementar el .cpp de bot y logger.
* Mundo.cpp y .h para establecer la comunicacion con los nuevos ejecutables así como para la finalización del programa cuando alguno de los dos jugadores llegue a 3 puntos.

## v2.1 2020-10-30

### Added
* Funcion disminuye para que la pelota vaya disminuyendo su radio a medida que aumenta el tiempo
* Readme.txt que contiene las reglas del juego

### Modificated
* Clase esfera y raqueta para posibilitar el movimiento


## v1.2 2020-10-13

Fin de la practica 1

## v1.1 2020-10-13

### Added
Cambio de cabecera





